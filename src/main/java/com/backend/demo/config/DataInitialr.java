package com.backend.demo.config;

import com.backend.demo.repository.UserRepository;
import com.backend.demo.entity.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class DataInitialr implements ApplicationListener<ContextRefreshedEvent> {

    @Autowired
    UserRepository userRepository;

    @Override
    public void onApplicationEvent(ContextRefreshedEvent contextRefreshedEvent) {

        List<User> users = userRepository.findAll();
        if(users.isEmpty()){
            this.createUsers( "felipe", "fealvesjulio@gmail.com", "123");
        }
    }

    public void createUsers(String name, String email, String password){
        User user = new User(name, email, password);
        userRepository.save(user);
    }
}
